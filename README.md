# darwin-password-generator

## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Type-Check, Compile and Minify for Production

```sh
npm run build
```

### Run End-to-End Tests with [Cypress](https://www.cypress.io/)

On the command line:

```sh
npx cypress run
```

Or inside the Cypress GUI:

```sh
npx cypress open
```
