export interface Password {
    character: string
    sourceBeginning: string
    sourceEnd: string
}
