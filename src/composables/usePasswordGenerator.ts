import { ref } from "vue"
import type { Password } from "@/types"

export function usePasswordGenerator() {
    const book = ref<string>("")
    const passwordLength = ref<number>(12)
    const checkboxNumbers = ref<boolean>(false)
    const checkboxSymbols = ref<boolean>(false)
    const password = ref<Array<Password>>([])

    const random32BitLimit = 4294967295

    async function loadBook() {
        return fetch("book.txt")
            .then((response) => response.text())
            .then((text) => {
                book.value = text
            })
    }

    function generatePassword() {
        password.value.length = 0
        const numberOfCharsInBook = book.value.length

        for (let i = 0; i < passwordLength.value; i++) {
            let char = " "
            let randomInt = 0
            const cutOffPoint = 120
            let capitalLetterHasBeenSelected = false
            let numberHasBeenSelected = false
            let symbolHasBeenSelected = false

            // If randomInt happens to land on a whitespace character,
            // try again with a new random integer.
            while (
                char === " " ||
                char === "\r" ||
                char === "\n" ||
                (!capitalLetterHasBeenSelected && i === 0) ||
                (!numberHasBeenSelected && i === 1 && checkboxNumbers.value) ||
                (!symbolHasBeenSelected && i === 2 && checkboxSymbols.value)
            ) {
                randomInt = getRandomInt(numberOfCharsInBook)
                char = book.value[randomInt]

                if (/[A-Z]/.test(char)) capitalLetterHasBeenSelected = true
                if (/\d/.test(char)) {
                    // Reject number if numbers checkbox is not checked
                    if (!checkboxNumbers.value) char = " "
                    else numberHasBeenSelected = true
                }
                if (/[(),.@$!%*“?—<>'|{}^&+=`~#_:’;-]/.test(char)) {
                    // Reject symbol if symbols checkbox is not checked
                    if (!checkboxSymbols.value) char = " "
                    else symbolHasBeenSelected = true
                }
            }

            const passwordObject: Password = {
                character: char,

                // Get part from source before and after password character
                sourceBeginning:
                    "..." +
                    book.value.slice(
                        // If randomInt is too close to the beginning of the book,
                        // set it to start at the beginning. Otherwise there would
                        // be an error.
                        randomInt - cutOffPoint < 1
                            ? 0
                            : randomInt - cutOffPoint,
                        randomInt
                    ),
                sourceEnd:
                    book.value.slice(
                        randomInt + 1,
                        // If randomInt is too close to the end of the book,
                        // set it to end at the end. Otherwise there would
                        // be an error.
                        randomInt + cutOffPoint > numberOfCharsInBook
                            ? numberOfCharsInBook
                            : randomInt + cutOffPoint
                    ) + "...",
            }
            password.value[i] = passwordObject
        }
        shufflePassword(password.value)
    }

    function getRandomInt(max: number) {
        let randomInt = 0
        const moduloBiasCutOff = random32BitLimit - (random32BitLimit % max)
        const random = window.crypto.getRandomValues(new Uint32Array(1))

        // Only accept integers that don't exceed the max parameter
        if (random[0] < moduloBiasCutOff) {
            randomInt = random[0] % max
        }
        return randomInt
    }

    function shufflePassword(array: Array<Password>) {
        for (let i = array.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1))
            ;[array[i], array[j]] = [array[j], array[i]]
        }
    }

    return {
        passwordLength,
        checkboxNumbers,
        checkboxSymbols,
        password,
        loadBook,
        generatePassword,
    }
}
