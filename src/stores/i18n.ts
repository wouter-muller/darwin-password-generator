import { defineStore } from "pinia"

export const useI18nStore = defineStore("i18n", {
    state: () => {
        return {
            heading: "Darwin's Password Generator",
            paragraph:
                'This password generator takes randomly selected characters from "The Origin of Species" by Charles Darwin. You can hover over each character to discover where in the book it came from. This is not only secure, but also inspiring!',
            passwordHeading: "Generated password:",
            buttonText: "Generate password",
            inputs: {
                lengthSlider: "Length",
                checkboxNumbers: "Numbers",
                checkboxSymbols: "Symbols",
            },
        }
    },
})
