import { defineConfig } from "cypress"

export default defineConfig({
    viewportWidth: 800,
    viewportHeight: 800,
    e2e: {
        specPattern: "cypress/e2e/**/*.{cy,spec}.{js,jsx,ts,tsx}",
        baseUrl: "http://127.0.0.1:5173",
    },
})
