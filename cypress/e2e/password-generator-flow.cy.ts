describe("Password generator flow", () => {
    it("loads the app and finds the correct heading and paragraph", () => {
        cy.visit("/")
        cy.get('[data-cy="heading"]').contains("Darwin's Password Generator")
        cy.get('[data-cy="paragraph"]')
    })

    it("shows a password wrapper, a refresh button, a length slider, a numbers checkbox, and a symbols checkbox", () => {
        cy.visit("/")
        cy.get('[data-cy="password"]')
        cy.get('[data-cy="button"]')
        cy.get('[data-cy="length-slider"]')
        cy.get('[data-cy="checkbox-numbers"]')
        cy.get('[data-cy="checkbox-symbols"]')
    })

    it('shows a different password after hitting the "Generate password" button', () => {
        cy.visit("/")
        cy.get('[data-cy="password"]').then(($oldPassword) => {
            const oldPassword = $oldPassword.text()

            cy.get('[data-cy="button"]').click()

            cy.get('[data-cy="password"]').should(($newPassword) => {
                expect($newPassword.text()).not.to.eq(oldPassword)
            })
        })
    })
})
